IF  EXISTS (SELECT * FROM sys.objects WHERE name = 'sp_list_solicitud_compra' AND type = 'P')
    DROP PROCEDURE sp_list_solicitud_compra
GO

----------------------------------------------------------------------------------
-- Author: JOSE CHUMIOQUE IDE-SOLUTION
-- Created: 26/01/2022  
-- Sistema: Web API Compras
-- Modulo: General
-- Descripción: Listar solicitudes
-- Log Modificaciones: 
-- CODIGO    REQUERIMIENTO     NOMBRE                   FECHA       MOTIVO  
-----------------------------------------------------------------------------------
-----------------------------------------------------------------------------------
CREATE PROCEDURE [dbo].[sp_list_solicitud_compra]
AS
	BEGIN
		BEGIN TRY		
			SET NOCOUNT ON;
					
					select 
			    ord.orde_id as ordenID,
				ord.orde_tipo as ordenTipo,
				ord.clie_id as ClienteID,
				pers_tipodoc as tipoDoc,
				(select conc_descripcion from tb_concepto where conc_correlativo = pers_tipodoc and conc_prefijo = 1) as descTipoDocumento,
				pers_documento as numDoc,
				pers_nombres +' '+ pers_apellpat + ' '+ pers_apellmat as nombresApellidos,
				pers_celular as celular,
				pers_email as email,
				pers_estadocivil as estadoCivil,
				prod.prod_categoria as categoria,
                (select cate_nombre from tb_categoria where cate_id = prod.prod_categoria) as inmueble
				 from tb_orden ord
				 inner join tb_cliente cli on cli.clie_id = ord.clie_id
				 inner join tb_persona per on per.pers_id = cli.pers_id
                 inner join tb_producto prod on prod.prod_id = ord.prod_id
				 where ord.orde_marcabaja = 0
				 and cli.clie_marcabaja = 0
				 and per.pers_marcabaja = 0
				 order by ord.orde_id;
									
				END TRY
				BEGIN CATCH

					SELECT   
					ERROR_NUMBER() AS ErrorNumber  
				   ,ERROR_MESSAGE() AS ErrorMessage;  
				END CATCH
	END	
GO
